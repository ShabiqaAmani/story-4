from django.urls import path
from . import views

app_name = 'kegiatan'

urlpatterns = [
    path('Activities/', views.activity, name='activities'),
    path('Activities/peserta/', views.peserta, name='peserta'),
    path('Activities/new/', views.new_activity, name='new_activity'),
    path('Activities/delete/<str:Nama_Kegiatan>', views.delete, name='delete'),

]
