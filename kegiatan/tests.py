from django.test import LiveServerTestCase, TestCase, tag, Client
from django.urls import resolve
from selenium import webdriver
from http import HTTPStatus
from .views import activity, new_activity,peserta

class MainTestCase(TestCase):

    def test_url_activities(self):
        response = Client().get('/Activities/')
        self.assertEqual(response.status_code, 200)
    
    def test_url_activities_using_kegiatan_template (self):
        response = Client().get('/Activities/')
        self.assertTemplateUsed(response, 'kegiatan/kegiatan.html')

    def test_url_activities_using_activity_func(self):
        found = resolve('/Activities/')
        self.assertEqual(found.func, activity)

    def test_url_new_activity(self):
        response = Client().get('/Activities/new/')
        self.assertEqual(response.status_code, 200)
    
    def test_url_new_activity_using_form_kegiatan_template (self):
        response = Client().get('/Activities/new/')
        self.assertTemplateUsed(response, 'kegiatan/formkegiatan.html')

    def test_url_new_activity_using_new_activity_func(self):
        found = resolve('/Activities/new/')
        self.assertEqual(found.func, new_activity)

    def test_apakah_halaman_form_kegiatan_di_redirect_ke_template(self):
        response = self.client.post(
            "/Activities/"
        )
        self.assertEqual(response.status_code, 200)

    def test_url_peserta(self):
        response = Client().get('/Activities/peserta/')
        self.assertEqual(response.status_code, 200)

    def test_url_peserta_using_peserta_template (self):
        response = Client().get('/Activities/peserta/')
        self.assertTemplateUsed(response, 'kegiatan/peserta.html')

    def test_url_peserta_using_peserta_func(self):
        found = resolve('/Activities/peserta/')
        self.assertEqual(found.func, peserta)
    

    def test_apakah_halaman_form_peserta_di_redirect_ke_template(self):
        response = self.client.post(
            "/Activities/"
        )
        self.assertEqual(response.status_code, 200)

    


    