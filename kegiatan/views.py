from django.shortcuts import render
from django.shortcuts import redirect
from .forms import formKegiatan, formProfile
from .models import Kegiatan


def activity(request):
    products = Kegiatan.objects.all
    return render(request, 'kegiatan/kegiatan.html', {'products':products})

def new_activity(request):
    if request.method == "POST":
        form = formKegiatan(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.save()
            return redirect('/Activities')
            
    else:
        form = formKegiatan()
    return render(request, 'kegiatan/formkegiatan.html', {'form': formKegiatan})

def peserta (request):
    if request.method == "POST":
        form = formProfile(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.save()
            return redirect('/Activities')
            
    else:
        form = formKegiatan()
    return render(request, 'kegiatan/peserta.html', {'form': formProfile})

def delete (request, Nama_Kegiatan):
    Kegiatan.objects.filter(Nama_Kegiatan= Nama_Kegiatan).delete()
    return redirect('/Activities')

# Create your views here.
