from django.test import TestCase
from django.test import LiveServerTestCase, TestCase, tag, Client
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.urls import reverse, resolve
from django.http import HttpRequest
from .views import signin, signout, register
from django.contrib.auth.models import User

# Create your tests here.
class Story9UnitTest(TestCase):

    def test_if_login_url_exist(self):
        response = Client().get('/login/')
        self.assertEquals(response.status_code, 200)

    def test_if_login_url_using_login_template(self):
        response = Client().get('/login/')
        self.assertTemplateUsed(response, "Story9/login.html")

    def test_url_login_using_signin_func(self):
        found = resolve('/login/')
        self.assertEqual(found.func, signin)

    def test_if_register_url_exist(self):
        response = Client().get('/register/')
        self.assertEquals(response.status_code, 200)

    def test_if_register_url_using_registration_template(self):
        response = Client().get('/register/')
        self.assertTemplateUsed(response, "Story9/registration.html")

    def test_url_register_using_register_func(self):
        found = resolve('/register/')
        self.assertEqual(found.func, register)
    
    def test_if_logout_url_redirected(self):
        response = Client().get('/logout/')
        self.assertEquals(response.status_code, 302)

    def test_url_logout_using_signout_func(self):
        found = resolve('/logout/')
        self.assertEqual(found.func, signout)
   
    def test_form_validation_accepted(self):
        self.credentials = {
            'username': 'testuser',
            'password': 'secret'}
        User.objects.create_user(**self.credentials)
        form = AuthenticationForm(data=self.credentials)
        self.assertTrue(form.is_valid())


