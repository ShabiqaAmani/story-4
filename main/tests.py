from django.test import LiveServerTestCase, TestCase, tag, Client
from django.urls import resolve
from selenium import webdriver
from main.views import home, profile, portfolio,add_class, new_class
from http import HTTPStatus
from .models import addmatkul


class MainTestCase(TestCase):

    def test_root_url_home(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_root_url_using_home_template (self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'main/home.html')

    def test_url_about (self):
        response = Client().get('/About/')
        self.assertEqual(response.status_code, 200)

    def test_url_about_using_template_profile (self):
        response = Client().get('/About/')
        self.assertTemplateUsed(response, 'main/Profile.html')

    def test_url_about_using_profile_func(self):
        found = resolve('/About/')
        self.assertEqual(found.func, profile)

    def test_url_portfolio (self):
        response = Client().get('/portfolio/')
        self.assertEqual(response.status_code, 200)

    def test_url_portfolio_using_template_portfolio (self):
        response = Client().get('/portfolio/')
        self.assertTemplateUsed(response, 'main/Portfolio.html')

    def test_url_portfolio_using_portfolio_func(self):
        found = resolve('/portfolio/')
        self.assertEqual(found.func, portfolio)

    def test_url_class (self):
        response = Client().get('/class/')
        self.assertEqual(response.status_code, 200)

    def test_url_class_using_template_matakuliah(self):
        response = Client().get('/class/')
        self.assertTemplateUsed(response, 'main/matakuliah.html')

    def test_url_class_using_add_class_func(self):
        found = resolve('/class/')
        self.assertEqual(found.func, add_class)

    def test_url_class_new(self):
        response = Client().get('/class/new/')
        self.assertEqual(response.status_code, 200)

    def test_url_class_new_using_new_class_func(self):
        found = resolve('/class/new/')
        self.assertEqual(found.func, new_class)

    def test_apakah_halaman_form_di_redirect_ke_template(self):
        response = self.client.post(
            "/class/new/"
        )
        self.assertEqual(response.status_code, 200)

    def test_model_can_create_mata_kuliah (self):
        new_mata_kuliah = addmatkul.objects.create (mata_kuliah = 'DDAK', dosen_pengajar = 'Anwar', jumlah_sks = 3, deskripsi_mata_kuliah = 'fun', semester_tahun = 'Gasal', ruang_kelas = 'r.2201')
        counting_all_available_mata_kuliah = addmatkul.objects.all().count()
        self.assertEqual (counting_all_available_mata_kuliah, 1)

    
    


   



