from django import forms
from .models import addmatkul

class MataKuliahform(forms.ModelForm):
	class Meta:
		model = addmatkul
		fields = ['mata_kuliah', 'dosen_pengajar', 'jumlah_sks', 'deskripsi_mata_kuliah', 'semester_tahun', 'ruang_kelas']
        
attrs = {'type' : 'text'}

error_messages = {
	'required' : 'Please Type'
}

	

mata_kuliah = forms.CharField(label='Nama mata kuliah', required=False, max_length=50, widget=forms.TextInput(attrs=attrs),  error_messages={
               'required': 'Please enter your name' })
dosen_pengajar = forms.CharField(label='Nama dosen', required=False, max_length=30, widget=forms.TextInput(attrs=attrs))
jumlah_sks = forms.CharField(label='Jumlah sks', required=False, widget=forms.TextInput(attrs=attrs))
deskripsi_mata_kuliah = forms.CharField(label='Deskripsi mata kuliah', required=False, max_length=600, widget=forms.Textarea(attrs=attrs))
semester_tahun= forms.CharField(label='Tahun ajaran', required=False, max_length=30, widget=forms.TextInput(attrs=attrs))
ruang_kelas = forms.CharField(label='Ruang kelas', required=False, max_length=20, widget=forms.TextInput(attrs=attrs))
 