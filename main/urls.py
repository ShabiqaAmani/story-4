from django.urls import path
from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('About/', views.profile, name='About'),
    path('portfolio/', views.portfolio, name='portfolio'),
    path('class/', views.add_class, name='add_class'),
    path('class/new/', views.new_class, name='new_class'),
    path('class/delete/<str:mata_kuliah>', views.delete, name='delete')

]
