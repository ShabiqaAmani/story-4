from django.shortcuts import render
from django.shortcuts import redirect
from .forms import MataKuliahform
from .models import addmatkul

def home(request):
    return render(request, 'main/home.html')

def profile(request):
    return render(request, 'main/Profile.html')

def portfolio (request):
    return render (request, 'main/Portfolio.html')

def add_class (request):
    products = addmatkul.objects.all
    return render (request, 'main/matakuliah.html', {'products':products})

def new_class (request):
   
    if request.method == "POST":
        form = MataKuliahform(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.save()
            return redirect('/class')
            
    else:
        form = MataKuliahform()
    return render(request, 'main/formmatkul.html', {'form': MataKuliahform})

def delete (request, mata_kuliah):
    addmatkul.objects.filter(mata_kuliah= mata_kuliah).delete()
    return redirect('/class')

